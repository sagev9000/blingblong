extends StaticBody2D

signal caught_ball

var caught = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body

func _on_CaughtBox_body_entered(body):
	emit_signal("caught_ball", caught)
	body.get_parent().remove_child(body)
	if !caught:
		$Bottom/Line.default_color = Color(0, 1, .2)
		$Left/Line.default_color = Color(0, 1, .2)
		$Right/Line.default_color = Color(0, 1, .2)
		caught = true
