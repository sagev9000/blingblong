extends Node2D

const DROP_SPEED = 5
const FLY_SPEED = 400

var current_audio_hit = 0
var audio_hits = []

var forward = true
onready var ball_scene = load("Ball.tscn")
onready var ball = get_node("Ball")

func _ready():
	fly(true)
	
	for child in get_children():
		if child.is_class("StaticBody2D"):
			print("BODY")
			print(child.name)
			child.connect("caught_ball", self, "caught_ball")
		if child.is_class("AudioStreamPlayer"):
			audio_hits.push_back(child)
	
func _process(_delta):
	if forward && ball.position.x > 600:
		fly(false)
	elif !forward && ball.position.x < -400:
		fly(true)

func fly(fward):
	if fward:
		ball.linear_velocity = Vector2(FLY_SPEED, 0)
	else:
		ball.linear_velocity = Vector2((-1)*FLY_SPEED, 0)
	forward = fward

func stop_flying():
	ball.linear_velocity = Vector2(0, 0)

func _on_Button_pressed():
	stop_flying()
	ball.sleeping = false
	ball.gravity_scale = DROP_SPEED
	ball.collision_layer = 1
	ball.collision_mask = 1
	
	ball = ball_scene.instance()
	add_child(ball)
	fly(true)

func caught_ball(already_caught):
	if !already_caught:
		audio_hits[current_audio_hit].play()
		current_audio_hit += 1
		if current_audio_hit == audio_hits.size():
			current_audio_hit = 0
